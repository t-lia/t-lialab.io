---
layout: page
title: Découvrir T-Lia
---


L’association T-Lia - association de fait depuis Mars 2019 - vise à développer et partager des *expérimentations* populaires de _**T**echnologies **Li**bres pour l'**a**utonomie collective_.

Les technologies libres, conviviales et/ou low-tech proposent un chemin vers plus de **sobriété**, de « **faire soi-même** », de **réemploi**, de recyclage, de **partage** et de **co-construction des savoirs** (dont l’utilisation, l’étude, la modification et la duplication par autrui en vue de sa diffusion sont permises et encouragées). Le but est de concevoir des outils non seulement **utiles** (qui visent à couvrir des besoins de base) et **accessibles** (pour faciliter la réappropriation) mais également **soutenables**, **robustes**, **réparables** et **fabriqués localement**.

Nous nous intéressons plus particulièrement aux questions relatives à nos consommations d’énergie et d’eau au quotidien, même si nos propositions sont diverses : réducteurs d’eau de douche, économiseurs de cuisson, isolants de ballon d’eau chaude, panneaux solaires chauffe-air, déshydrateur solaire, vélo générateur d’électricité, ordinateur minimaliste, outils multifonctions à pédales …

Notre approche se veut **pratique et ludique** afin de favoriser l’**implication**, la **coopération** et la **créativité** des participant.e.s. Nous portons donc une attention particulière aux **dynamiques de groupes** et mobilisons l'intelligence collective des participants en utilisant la posture et les méthodes de l'**éducation populaire**.

![](/assets/images/alternateur.JPG)

Nous proposons:

- des **temps de découvertes, partages d’expériences, réflexions et débats collectifs** sur les implications écologiques des technologies mais aussi leurs enjeux sociaux et sociétaux – beaucoup moins présents dans le débat public.

- des **ateliers de fabrication** de technologies libres. L’approche de T-Lia pourrait être qualifiée d’**éducation populaire manuelle**. Les animateur.ice.s de l’association sont là pour favoriser l’appropriation et la coopération entre les participant.e.s

- **Montage de projets autour des technologies libres : _conseil, diagnostic, accompagnement, co-conception de solutions, co-fabrication de technologies libres, recherche action_**

Nous avons tissé de nombreux liens avec des partenaires locaux et nationaux et allons initier une communauté locale à Toulouse à partir du mois de Mars 2020.

Toutes nos actions sont conçues pour s’adresser à tous.te.s, peu importe le niveau de connaissances.

![](/assets/images/vélo.JPG)
